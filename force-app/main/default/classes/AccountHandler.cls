public class AccountHandler {
    public static Account insertNewAccount(String nomeConta){
        Account conta = new Account();
        conta.Name = nomeConta;
        try {
            insert conta;
        }
       catch (DmlException e) {
    			
			System.debug('A DML exception has occurred: ' + e.getMessage());
            return null ;
        }
        return conta;
    }
}