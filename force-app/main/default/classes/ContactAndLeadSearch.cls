public class ContactAndLeadSearch {
    public static List<List<SObject>> searchContactsAndLeads(String sobrenome){
        List<List<SObject>> LeadContatos = [
            FIND :sobrenome  IN ALL FIELDS 
            RETURNING Contact(FirstName, LastName), Lead(FirstName,LastName)
        ];
        return LeadContatos;
    }

}