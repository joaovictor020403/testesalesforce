@IsTest
private class AccountManagerTest {
    @isTest 
    static void testGetAccount() {
        
        Account a = new Account(Name='Teste Conta');
        insert a;
        
        Contact c = new Contact(AccountId=a.Id,  FirstName = 'Conta', LastName='Teste');
        insert c;
        
        RestRequest request = new RestRequest();
        request.requestUri =
            'https://yourInstance.salesforce.com/services/apexrest/Accounts/'
            + a.Id + '/contacts';
        request.httpMethod = 'GET';
        RestContext.request = request;

        Account contaTeste = AccountManager.getAccount();
        // Verify results
        System.assert(contaTeste != null);
        System.assertEquals('Teste Conta', contaTeste.Name);
    }
}