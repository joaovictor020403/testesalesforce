@RestResource(urlMapping='/Accounts/*/contacts')
global with sharing class AccountManager {
    @HttpGet
    global static Account getAccount() {
        RestRequest request = RestContext.request;
       
        String caseId = request.requestURI.substring(
          request.requestURI.lastIndexOf('/')+1);
        
        String idConta = request.requestURI.substringBetween('Accounts/','/contacts');
        
        Account result =  [SELECT Id, Name,(SELECT Id, FirstName, LastName FROM Contacts) 
                           FROM Account
                           WHERE Id = :idConta];
        return result;
    }
}