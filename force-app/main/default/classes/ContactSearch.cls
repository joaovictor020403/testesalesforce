public class ContactSearch {
    public static List<Contact> searchForContacts(String sobrenome, String codigoPostal){
        List<Contact> Contatos = [select name from Contact where LastName =: sobrenome 
                                 and MailingPostalCode =: codigoPostal
                                 ];
            return Contatos;
    }
}