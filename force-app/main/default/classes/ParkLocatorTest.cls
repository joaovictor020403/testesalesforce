@isTest
private class ParkLocatorTest {

    @isTest static void testCallout() {
        Test.setMock(WebServiceMock.class, new ParkServiceMock());
		
		List<String> resultado = new List<String>();
        List<String> valorEsperado = new List<String>{'Germany','India','Japan'};
		
        resultado = ParkLocator.country('Teste');
		System.assertEquals(valorEsperado, resultado); 
    }
}